===============================
Python Client for dutate.com
===============================

Install
-------

``pip install dutate``


Usage
-----

::

    from dutate import Dutate

    dtt = Dutate(token='ave32sde98ruj23if3riugrg')

    dtt.track('important event')
